# include <mpi.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <time.h>
# include <omp.h>

int main ( int argc, char *argv[] );
double f ( double x );

int main ( int argc, char *argv[] ) {
  double a; 
  double b; 
  double error;
  double exact = 0.49936338107645674464;
  int i;
  int n;
  int temp;
  double total;
  double wtime;
  double x;
  int next,start,end,prev,rank,size,tag,send,rec;
  double rec1,n1;
  MPI_Status status;
  
  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  MPI_Comm_size(MPI_COMM_WORLD, &size);



  
  
  next = (rank+1)%size;
  prev = (rank+size-1)%size;

  


  if (rank == 0)
  {
    if (argc != 4) {
      n = 10000000;
      a = 0.0;
      b = 10.0;
    } else {
      n = atoi(argv[1]);
      a = atoi(argv[2]);
      b = atoi(argv[3]);
    }

    printf ( "\n" );
    printf ( "QUAD:\n" );
    printf ( "  Estimate the integral of f(x) from A to B.\n" );
    printf ( "  f(x) = 50 / ( pi * ( 2500 * x * x + 1 ) ).\n" );
    printf ( "\n" );
    printf ( "  A        = %f\n", a );
    printf ( "  B        = %f\n", b );
    printf ( "  N        = %d\n", n );
    printf ( "  Exact    = %24.16f\n", exact );
  
    wtime = omp_get_wtime ( );
    for (temp=1;temp<size;temp++)
    {
      MPI_Send(&a, 1, MPI_DOUBLE, temp, tag, MPI_COMM_WORLD); 
    }
    for (temp=1;temp<size;temp++)
    {
      MPI_Send(&b, 1, MPI_DOUBLE, temp, tag, MPI_COMM_WORLD); 
    }
    for (temp=1;temp<size;temp++)
    {
      MPI_Send(&n, 1, MPI_INT, temp, tag, MPI_COMM_WORLD); 
    }
  
  }
  else
  {
    MPI_Recv(&a, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
    MPI_Recv(&b, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
    MPI_Recv(&n, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);
  }

  
  total = 0.0;

  n1 =n/size;


  if (n%size!=0)
  {
    n1++;
  }


  if (rank == 0)
  {

    rec=0;
    for (temp=1;temp<size;temp++)
    {
      rec+=n1;
      MPI_Send(&rec, 1, MPI_INT, temp, tag, MPI_COMM_WORLD);
    }
    rec = n1;
    for (temp=1;temp<size;temp++)
    {
      rec+=n1;
      if (rec>n)
      {
        rec = n;
      }
      MPI_Send(&rec, 1, MPI_INT, temp, tag, MPI_COMM_WORLD);
    }

    
    
    for ( temp = 0; temp < n1; temp++ )
    {
      x = ( ( double ) ( n - temp - 1 ) * a + ( double ) ( temp ) * b ) / ( double ) ( n - 1 );
      total = total + f ( x );
    }

    for (temp=1;temp<size;temp++)
    {
      MPI_Recv(&rec1, 1, MPI_DOUBLE, temp, tag, MPI_COMM_WORLD, &status);
      total+=rec1;
    }
    
    wtime = omp_get_wtime ( ) - wtime;

    total = ( b - a ) * total / ( double ) n;
    error = fabs ( total - exact );
  
    printf("\n" );
    printf ( "  Estimate = %24.16f\n", total );
    printf ( "  Error    = %e\n", error );
    printf ( "  Time     = %f\n", wtime );
    printf ( "\n" );
    printf ( "  Normal end of execution.\n" );
    printf ( "\n" );

  }
  else
  {
    MPI_Recv(&start, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);
    MPI_Recv(&end, 1, MPI_INT, 0, tag, MPI_COMM_WORLD, &status);

    for ( i = start; i < end; i++ )
    {
      x = ( ( double ) ( n - i - 1 ) * a + ( double ) ( i ) * b ) / ( double ) ( n - 1 );
      total = total + f ( x );
    }

    MPI_Send(&total, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
    
  }

  MPI_Finalize();
  

  return 0;
}


double f ( double x ) {
  double pi = 3.141592653589793;
  double value;

  value = 50.0 / ( pi * ( 2500.0 * x * x + 1.0 ) );

  
  return value;
}

